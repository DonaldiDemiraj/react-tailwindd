import React from 'react'
import ImageOne from '../images/noodles.jpg'
import ImageTwo from '../images/curry.jpg'

function Content() {
    return (
        <>
            <div className='menu-card'>
                <img src={ImageOne} alt="egg" className="h-full rounded mb-20 shadow" />
                <div className="center-content">
                    <h2 className="text-2xl mb-2">Curry</h2>
                    <p className="mb-2">Lorem ferwgtoerjhgerg[wjgtew]gjrwhgj]wjhgrw]hg</p>
                    <span>$16</span>
                </div>
            </div>
            <div className='menu-card'>
                <img src={ImageTwo} alt="egg" className="h-full rounded mb-20 shadow" />
                <div className="center-content">
                    <h2 className="text-2xl mb-2">Noodles</h2>
                    <p className="mb-2">Lorem ferwgtoerjhgerg[wjgtew]gjrwhgj]wjhgrw]hg</p>
                    <span>$30</span>
                </div>
            </div>
        </>
    )
}

export default Content
